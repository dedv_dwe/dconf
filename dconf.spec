Name:           dconf
Version:        0.40.0
Release:        3
Summary:        Dconf provides a backend to the GSettings API in Glib

License:        LGPLv2+ and GPLv2+ and GPLv3+
URL:            http://live.gnome.org/%{name}
Source0:        http://download.gnome.org/sources/%{name}/0.40/%{name}-%{version}.tar.xz

Patch9000:	memset-chunk-date-to-eliminate-discrepancy.patch

BuildRequires:  bash-completion dbus-devel glib2-devel >= 2.44.0 gtk-doc meson vala libxslt
Requires:       dbus glib2%{?_isa} >= 2.44.0

%description
Dconf is a low-level configuration system. Its main purpose is to provide a backend
to GSettings on platforms that don't already have configuration storage systems.

%package devel
Summary: Development files for dconf
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
Includes header files, static library and other development files for dconf.

%package help
Summary: Document files for dconf

%description help
This package contains some readme, man and other related  files for dconf.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%meson -Dgtk_doc=true
%meson_build

%check
%meson_test

%install
%meson_install


install -d $RPM_BUILD_ROOT%{_sysconfdir}/dconf/profile
install -d $RPM_BUILD_ROOT%{_sysconfdir}/dconf/db/{site,local,distro}.d/locks

cat << EOF > $RPM_BUILD_ROOT%{_sysconfdir}/%{name}/profile/user
user-db:user
system-db:local
system-db:site
system-db:distro
EOF

%posttrans
dconf update

%files
%license COPYING
%dir %{_sysconfdir}/dconf
%dir %{_sysconfdir}/dconf/db
%dir %{_sysconfdir}/dconf/db/local.d
%dir %{_sysconfdir}/dconf/db/local.d/locks
%dir %{_sysconfdir}/dconf/db/site.d
%dir %{_sysconfdir}/dconf/db/site.d/locks
%dir %{_sysconfdir}/dconf/db/distro.d
%dir %{_sysconfdir}/dconf/db/distro.d/locks
%dir %{_sysconfdir}/dconf/profile
%{_libexecdir}/dconf-service
%{_bindir}/dconf
%{_libdir}/libdconf.so.1*
%{_libdir}/gio/modules/libdconfsettings.so
%{_datadir}/bash-completion/completions/dconf
%{_datadir}/dbus-1/services/ca.desrt.dconf.service
%config(noreplace) %{_sysconfdir}/dconf/profile/user
%{_userunitdir}/dconf.service

%files devel
%{_includedir}/dconf
%{_libdir}/libdconf.so
%{_libdir}/pkgconfig/dconf.pc
%{_datadir}/vala
%{_datadir}/gtk-doc/html/dconf
%dir %{_datadir}/gtk-doc
%dir %{_datadir}/gtk-doc/html

%files help
%{_mandir}/man1/dconf-service.1.gz
%{_mandir}/man1/dconf.1.gz
%{_mandir}/man7/dconf.7.gz

%changelog
* Thu Apr 7 2022 liuyumeng <liuyumeng@h-partners.com> -0.40.0-3
- enable tests,remove redundant files

* Tue Jan 18 2022 liuyumeng <liuyumeng5@huawei.com> - 0.40.0-2
- add patch:dconf read random contents of memory causing discrepancy,initialize memory to eliminate discrepancy

* Thu Dec 09 2021 liuyumeng <liuyumeng5@huawei.com> -0.40.0-1
- update to 0.40.0

* Sat Jan 30 2021 yanglu <yanglu60@huawei.com> - 0.38.0-1
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Upgrade to 0.38.0

* Tue Jun 30 2020 wenzhanli <wenzhanli2@huawei.com> - 0.36.0-1
- Type:bugfix
- ID:NAr
- SUG:NAr
- DESC:Upgrade to 0.36.0

* Mon Jun 29 2020 wenzhanli <wenzhanli2@huawei.com> - 0.34.0-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:optimize test cases bug

* Thu Jan 9 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.34.0-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:upgrade to 0.34.0

* Wed Sep 19 2019 yanzhihua <yanzhihua4@huawei.com> - 0.30.0-2
- Package init.

